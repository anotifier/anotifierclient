package ru.will0376.anotifier.utils.helpers;

import ru.will0376.anotifier.Config;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.AlgorithmParameters;
import java.security.spec.KeySpec;
import java.util.HashMap;
import java.util.Map;

public class EncryprionHelper {
	public static Map<byte[], byte[]> encryptText(SecretKey key, String in) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key);
		AlgorithmParameters params = cipher.getParameters();
		byte[] iv = params.getParameterSpec(IvParameterSpec.class).getIV();
		byte[] ciphertext = cipher.doFinal(in.getBytes(StandardCharsets.UTF_8));
		return new HashMap<byte[], byte[]>() {{
			put(iv, ciphertext);
		}};
	}

	public static String decryptText(SecretKey key, byte[] encText, byte[] iv) throws Exception {
		Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
		return new String(cipher.doFinal(encText), StandardCharsets.UTF_8);
	}

	public static SecretKey getServerEncKey() throws Exception {
		return getEncKey(Config.get().getServerSecretKey());
	}

	public static SecretKey getClientEncKey() throws Exception {
		return getEncKey(Config.get().getClientSecretKey());
	}

	private static SecretKey getEncKey(String in) throws Exception {
		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec spec = new PBEKeySpec(in.toCharArray(), in.getBytes(StandardCharsets.UTF_8), 65536, 256);
		SecretKey tmp = factory.generateSecret(spec);
		return new SecretKeySpec(tmp.getEncoded(), "AES");
	}
}
