package ru.will0376.anotifier.ui;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.fragment.app.Fragment;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.R;
import ru.will0376.anotifier.net.data.RequestData;
import ru.will0376.anotifier.utils.OnClickFragments;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;
import ru.will0376.anotifier.utils.helpers.LogHelper;

import java.util.HashMap;
import java.util.Map;

public class LogFragment extends Fragment {
	public static Map<String, LogStatus> preLog = new HashMap<>();
	public static LogFragment instance;
	public TextView logger;
	public NotificationManager notificationManager;

	public static void addToPreLog(String s, LogStatus ls) {
		preLog.put(s, ls);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		notificationManager = Drawer.instance.getSystemService(NotificationManager.class);
		instance = this;
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_log, container, false);
		TextView viewById = root.findViewById(R.id.logView);

		if (logger != null) viewById.setText(logger.getText());
		logger = viewById;
		logger.setMovementMethod(LogHelper.movementMethod);
		for (Map.Entry<String, LogStatus> entry : preLog.entrySet())
			Drawer.appendToLog(entry.getKey(), entry.getValue());
		OnClickFragments.registerTagFragment(root, this);
		return root;
	}

	public void clearLog(View view) {
		logger.setText("");
	}

	public void testNotify(View view) {
		NotificationCompat.Builder builder = new NotificationCompat.Builder(view.getContext(), "ANotifierChannel").setSmallIcon(R.drawable.bell)
				.setContentTitle(Drawer.instance.getTitle())
				.setContentText("Test notification")
				.setPriority(NotificationCompat.PRIORITY_DEFAULT)
				.setAutoCancel(true)
				.setContentIntent(PendingIntent.getActivity(view.getContext(), 0, new Intent(), 0))
				.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.bell));

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationChannel channel = new NotificationChannel("ANotifierChannel", "testNotify", NotificationManager.IMPORTANCE_DEFAULT);
			channel.setDescription("test notification");
			notificationManager.createNotificationChannel(channel);
		}
		notificationManager.notify(1, builder.build());
		Drawer.appendToLog("New test notifier", LogStatus.CreateTestNotify);
		if (ConnectionHelper.connected) {
			RequestData test = RequestData.builder()
					.action(RequestData.Action.Ping)
					.encData("Test")
					.encryptClientData()
					.setUUID()
					.build();
			ConnectionHelper.nettyClient.handler.sendMessage(test);
			Drawer.appendToLog("Sended!", LogStatus.Connection);
		}
	}

	public enum LogStatus {
		CreateTestNotify,
		CatchNotify,
		Other,
		Error,
		Connection,
		ConnectionError,
		CheckPermission,
		Wifi,
		AutoReconnect,
		Exception
	}
}