package ru.will0376.anotifier.net;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import lombok.Data;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.net.decoder.RequestDataEncoder;
import ru.will0376.anotifier.net.decoder.ResponseDataDecoder;
import ru.will0376.anotifier.tasks.CheckConnectionTask;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;

@Data
public class NettyClient implements Runnable {
	public String host;
	public int port;
	public EventLoopGroup workerGroup;
	public Bootstrap bootstrap;
	public ChannelFuture future;
	public ClientHandler handler;

	public NettyClient(String host) {
		this.host = host.split(":")[0];
		this.port = Integer.parseInt(host.split(":")[1]);
	}

	@Override
	public void run() {
		try {
			handler = new ClientHandler();
			bootstrap = new Bootstrap();
			bootstrap.group(workerGroup = new NioEventLoopGroup());
			bootstrap.channel(NioSocketChannel.class);
			bootstrap.option(ChannelOption.SO_KEEPALIVE, true);
			bootstrap.handler(new ChannelInitializer<SocketChannel>() {
				@Override
				public void initChannel(SocketChannel ch) {
					ch.pipeline()
							.addLast(new RequestDataEncoder(), new ResponseDataDecoder(), new ProcessingHandler(), handler);
				}
			});
			future = bootstrap.connect(host, port).sync();
			future.channel().closeFuture().sync();
		} catch (Exception ex) {
			ex.printStackTrace();
			Drawer.appendToLog("Error: " + ex.getCause(), LogFragment.LogStatus.ConnectionError);

			if (Config.get().isAutoReConnect() && CheckConnectionTask.canTryConnect())
				Drawer.appendToLog(String.format("Delay: %s sec", CheckConnectionTask.delay / 1000), LogFragment.LogStatus.AutoReconnect);
			CheckConnectionTask.incrementErrorCount();
			CheckConnectionTask.tryConnection = false;
		} finally {
			stopAll();
		}
	}

	public void stopAll() {
		try {
			workerGroup.shutdownGracefully().sync();
			ConnectionHelper.nettyClient = null;
			if (ConnectionHelper.nettyClientThread != null) {
				ConnectionHelper.nettyClientThread.interrupt();
				ConnectionHelper.nettyClientThread = null;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
