package ru.will0376.anotifier.net.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import ru.will0376.anotifier.net.data.ResponseData;

import java.nio.charset.StandardCharsets;
import java.util.List;

public class ResponseDataDecoder extends ReplayingDecoder<ResponseData> {
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
		ResponseData data = new ResponseData();
		data.setAction(ResponseData.Action.values()[in.readInt()]);
		int length = in.readInt();
		data.setEncData(in.readCharSequence(length, StandardCharsets.UTF_8).toString());
		out.add(data);
	}
}
