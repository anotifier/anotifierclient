package ru.will0376.anotifier.net;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import ru.will0376.anotifier.net.data.ResponseData;

public class ProcessingHandler extends ChannelInboundHandlerAdapter {
	@Override
	public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
		ResponseData responseData = (ResponseData) msg;
		ResponseHandler.handle(responseData);
		super.channelRead(ctx, msg);
	}
}
