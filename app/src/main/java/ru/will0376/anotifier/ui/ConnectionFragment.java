package ru.will0376.anotifier.ui;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import org.jetbrains.annotations.NotNull;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.R;
import ru.will0376.anotifier.tasks.CheckConnectionTask;
import ru.will0376.anotifier.utils.NotificationService;
import ru.will0376.anotifier.utils.OnClickFragments;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;
import ru.will0376.anotifier.utils.helpers.PermissionHelper;

import static ru.will0376.anotifier.Drawer.fastSaveConfig;

public class ConnectionFragment extends Fragment implements CompoundButton.OnCheckedChangeListener, TextWatcher {


	public static void openNotificationLSButton(View view) {
		PermissionHelper.openIntent(NotificationService.getIntentNotListSettings());
	}

	public static void checkPermissionsButton(View view) {
		PermissionHelper.requestAppPermissions();
	}

	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_connection, container, false);
		OnClickFragments.registerTagFragment(root, this);
		return root;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	public void connectToServer(View view) {
		fastSaveConfig();
		CheckConnectionTask.resetErrorCount();
		ConnectionHelper.connect();
	}

	public void disconnect(View view) {
		ConnectionHelper.disconnect(false);
	}

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		int id = buttonView.getId();
		if (id == R.id.wifiChecker) {
			Config.get().setUseWifiChecker(isChecked);
			fastSaveConfig();
		} else if (id == R.id.autoReconnect) {
			Config.get().setAutoReConnect(isChecked);
			fastSaveConfig();
			Drawer.instance.setupTimer();
		}
	}

	@Override
	public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setRetainInstance(true);

		((EditText) view.findViewById(R.id.serverIp)).setText(Config.get().getServerIp());
		((EditText) view.findViewById(R.id.clientKey)).setText(Config.get().getClientSecretKey());
		((EditText) view.findViewById(R.id.serverKey)).setText(Config.get().getServerSecretKey());
		((EditText) view.findViewById(R.id.maxConnectionTry)).setText(String.valueOf(Config.get()
				.getMaxConnectionTry()));

		((EditText) view.findViewById(R.id.serverIp)).addTextChangedListener(this);
		((EditText) view.findViewById(R.id.clientKey)).addTextChangedListener(this);
		((EditText) view.findViewById(R.id.serverKey)).addTextChangedListener(this);
		((EditText) view.findViewById(R.id.maxConnectionTry)).addTextChangedListener(this);

		((Switch) view.findViewById(R.id.wifiChecker)).setChecked(Config.get().isUseWifiChecker());
		((Switch) view.findViewById(R.id.autoReconnect)).setChecked(Config.get().isAutoReConnect());

		((Switch) view.findViewById(R.id.wifiChecker)).setOnCheckedChangeListener(this);
		((Switch) view.findViewById(R.id.autoReconnect)).setOnCheckedChangeListener(this);
	}

	@Override
	public void beforeTextChanged(CharSequence s, int start, int count, int after) {
	}

	@Override
	public void onTextChanged(CharSequence s, int start, int before, int count) {
	}

	@Override
	public void afterTextChanged(Editable s) {
		FragmentActivity activity = getActivity();
		if (activity != null && activity.getCurrentFocus() != null) {
			int id = activity.getCurrentFocus().getId();
			if (id == R.id.serverIp) {
				Config.get().setServerIp(String.valueOf(s));
			}
			if (id == R.id.serverKey) {
				Config.get().setServerSecretKey(String.valueOf(s));
			}
			if (id == R.id.clientKey) {
				Config.get().setClientSecretKey(String.valueOf(s));
			}
			if (id == R.id.maxConnectionTry) {
				Config.get().setMaxConnectionTry(Integer.parseInt(String.valueOf(s)));
			}
		}
	}
}