package ru.will0376.anotifier;

import android.app.NotificationManager;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import com.google.android.material.navigation.NavigationView;
import ru.will0376.anotifier.broadcasts.WifiReceiverStatus;
import ru.will0376.anotifier.net.NettyClient;
import ru.will0376.anotifier.tasks.CheckConnectionTask;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.ExceptionHandler;
import ru.will0376.anotifier.utils.NotificationService;
import ru.will0376.anotifier.utils.OnClickFragments;
import ru.will0376.anotifier.utils.RootUtil;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;
import ru.will0376.anotifier.utils.helpers.LogHelper;
import ru.will0376.anotifier.utils.helpers.PermissionHelper;

import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Timer;

import static ru.will0376.anotifier.ui.LogFragment.LogStatus.Other;

public class Drawer extends AppCompatActivity {
	public static Drawer instance;
	public static boolean debug = false;
	public static Handler handler;
	public NotificationManager notificationManager;

	public AppBarConfiguration mAppBarConfiguration;
	public static String version = String.format("%s.%s-%s", BuildConfig.VERSION_NAME, BuildConfig.VERSION_CODE, BuildConfig.BUILD_TYPE);

	public static void appendToLog(String text, LogFragment.LogStatus status) {
		LogHelper.lateAppendToLog(text, status);
	}

	public static void fastSaveConfig() {
		try {
			Config.get().saveConfig();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static boolean execCommands(String... command) {
		try {
			Runtime rt = Runtime.getRuntime();
			Process process = rt.exec("su");
			DataOutputStream os = new DataOutputStream(process.getOutputStream());

			for (String s : command) {
				os.writeBytes(s + "\n");
				os.flush();
			}
			os.writeBytes("exit\n");
			os.flush();
			process.waitFor();
		} catch (IOException | InterruptedException e) {
			return false;
		}
		return true;
	}

	public static boolean reGrandNotPerm() {
		if (RootUtil.isDeviceRooted()) {
			boolean b = execCommands("cmd notification disallow_listener ru.will0376.anotifier/ru.will0376.anotifier.utils.NotificationService", "cmd notification allow_listener ru.will0376.anotifier/ru.will0376.anotifier.utils.NotificationService");
			appendToLog("Done! granted permission:" + b, LogFragment.LogStatus.CheckPermission);
			return true;
		}
		appendToLog("Root not found", LogFragment.LogStatus.CheckPermission);
		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Thread.setDefaultUncaughtExceptionHandler(new ExceptionHandler());
		instance = this;
		super.onCreate(savedInstanceState);
		PermissionHelper.requestAppPermissions();

		Config.init(new File(getDataDir(), "files"));

		setContentView(R.layout.activity_drawer);
		Toolbar toolbar = findViewById(R.id.toolbar);
		setSupportActionBar(toolbar);
		DrawerLayout drawer = findViewById(R.id.drawer_layout);
		NavigationView navigationView = findViewById(R.id.nav_view);
		mAppBarConfiguration = new AppBarConfiguration.Builder(R.id.nav_log, R.id.nav_connection, R.id.nav_wifisettings, R.id.nav_notblacklist)
				.setDrawerLayout(drawer)
				.build();
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
		NavigationUI.setupWithNavController(navigationView, navController);

		startService(new Intent(this, NotificationService.class));
		notificationManager = getSystemService(NotificationManager.class);

		ConnectionHelper.nettyClient = new NettyClient(Config.get().getServerIp());
		ConnectionHelper.nettyClientThread = new Thread(ConnectionHelper.nettyClient);
		setupTimer();
		//registerWifiBroadcast(); //TODO: Исправить для автореконнекта.
		handler = new Handler(instance.getMainLooper());
		appendToLog(String.format("ver: %s", version), Other);
	}

	public void registerWifiBroadcast() {
		IntentFilter filters = new IntentFilter("android.net.wifi.WIFI_STATE_CHANGED");
		filters.addAction("android.net.wifi.STATE_CHANGE");
		registerReceiver(new WifiReceiverStatus(), filters);
	}

	public void setupTimer() {
		try {
			if (ConnectionHelper.timer != null) ConnectionHelper.timer.cancel();
			ConnectionHelper.timer = new Timer();
			if (Config.get().isAutoReConnect())
				ConnectionHelper.timer.scheduleAtFixedRate(ConnectionHelper.timerTask, 10000, CheckConnectionTask.delay);
		} catch (Exception ex) {
			appendToLog(ex.getLocalizedMessage(), LogFragment.LogStatus.Exception);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.drawer, menu);
		return true;
	}

	@Override
	public boolean onSupportNavigateUp() {
		NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
		return NavigationUI.navigateUp(navController, mAppBarConfiguration) || super.onSupportNavigateUp();
	}

	public void testNotify(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void clearLog(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void connectToServer(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void disconnect(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void checkPermissionsButton(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void openNotificationLSButton(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void addToListButton(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void refreshWifiList(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void removeSelectedFromSavedList(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void addNew(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void removeSelected(View view) {
		OnClickFragments.invokeFragmentButtonHandlerNoExc(view);
	}

	public void connectToServer(MenuItem item) {
		fastSaveConfig();
		CheckConnectionTask.resetErrorCount();
		ConnectionHelper.connect();
	}

	public void disconnect(MenuItem item) {
		ConnectionHelper.disconnect(false);
	}

	public void reGrandNotPerm(MenuItem item) {
		reGrandNotPerm();
	}

	public void sendPing(MenuItem item) {
		ConnectionHelper.sendPing();
	}
}