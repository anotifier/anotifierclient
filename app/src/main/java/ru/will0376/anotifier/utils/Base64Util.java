package ru.will0376.anotifier.utils;

import android.util.Base64;

import java.nio.charset.StandardCharsets;


public class Base64Util {
	public static String encode(String in) {
		return Base64.encodeToString(in.getBytes(StandardCharsets.UTF_8), Base64.URL_SAFE & Base64.NO_WRAP);
	}

	public static String encode(byte[] in) {
		return Base64.encodeToString(in, Base64.URL_SAFE & Base64.NO_WRAP);
	}

	public static String decode(String in) {
		return new String(Base64.decode(in, Base64.DEFAULT));
	}

	public static String decode(byte[] in) {
		return new String(Base64.decode(in, Base64.URL_SAFE & Base64.NO_WRAP));
	}

	public static byte[] decodeToByte(String in) {
		return Base64.decode(in, Base64.URL_SAFE & Base64.NO_WRAP);
	}
}
