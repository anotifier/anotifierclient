package ru.will0376.anotifier.net.data;

import com.google.gson.JsonObject;
import lombok.Builder;
import lombok.Data;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.utils.Base64Util;
import ru.will0376.anotifier.utils.helpers.EncryprionHelper;

import java.util.Map;

@Data
@Builder
public class RequestData {
	int id;
	@Builder.Default
	Device device = Device.Android;
	Action action;
	String encData;
	String uuid;
	@Builder.Default
	boolean encrypted = false;

	public enum Action {
		Ping,
		NewNotify
	}

	public enum Device {
		Android,
		Catcher
	}

	public static class RequestDataBuilder {
		public static int idCounter = 0;

		public RequestDataBuilder encryptClientData() {
			try {
				Map<byte[], byte[]> map = EncryprionHelper.encryptText(EncryprionHelper.getClientEncKey(), encData);
				map.forEach((key, value) -> {
					JsonObject jsonObject = new JsonObject();
					jsonObject.addProperty("iv", Base64Util.encode(key));
					jsonObject.addProperty("encData", Base64Util.encode(value));
					encData(Base64Util.encode(jsonObject.toString()));
				});
				encrypted(true);
				id(idCounter++);

			} catch (Exception e) {
				e.printStackTrace();
			}
			return this;
		}

		public RequestDataBuilder setUUID() {
			uuid(Config.get().getUuid());
			return this;
		}
	}
}
