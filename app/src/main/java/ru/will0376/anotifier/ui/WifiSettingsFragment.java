package ru.will0376.anotifier.ui;

import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import org.jetbrains.annotations.NotNull;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.R;
import ru.will0376.anotifier.broadcasts.WifiReceiverSSIDs;
import ru.will0376.anotifier.utils.OnClickFragments;

import java.util.List;
import java.util.stream.Collectors;

import static android.content.Context.WIFI_SERVICE;

public class WifiSettingsFragment extends Fragment {
	public static WifiSettingsFragment instance;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		instance = this;
	}

	public void fillSSIDList() {
		Spinner ssidList = getActivity().findViewById(R.id.ssidList);
		if (isWifiEnabled() && ssidList != null) {
			List<String> WifiList = getWIFIManager().getScanResults()
					.stream()
					.map(e -> e.SSID + "|" + e.BSSID)
					.collect(Collectors.toList());
			ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, WifiList);
			stringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			ssidList.setAdapter(stringArrayAdapter);

			String ssid = getWIFIManager().getConnectionInfo().getSSID().replaceAll("\"", "");
			if (WifiList.contains(ssid)) {
				ssidList.setSelection(WifiList.indexOf(ssid));
			}
		}
	}

	public String getSelectedSSID() {
		Spinner ssidList = getActivity().findViewById(R.id.ssidList);
		Object selectedItem = ssidList.getSelectedItem();
		if (selectedItem != null) {
			return (String) selectedItem;
		}
		return null;
	}

	public String getSelectedSavedSSID() {
		Spinner ssidList = getActivity().findViewById(R.id.savedWiFiList);
		Object selectedItem = ssidList.getSelectedItem();
		if (selectedItem != null) {
			return (String) selectedItem;
		}
		return null;
	}

	public void addNewSSIDToSavedList(String ssid) {
		Config.get().getSSIDList().add(ssid);
	}

	public void fillSavedWIFIList() {
		Spinner savedWiFiList = getActivity().findViewById(R.id.savedWiFiList);
		ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Config.get()
				.getSSIDList());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		savedWiFiList.setAdapter(adapter);
	}

	public WifiManager getWIFIManager() {
		return ((WifiManager) this.getActivity().getApplicationContext().getSystemService(WIFI_SERVICE));
	}

	public boolean isWifiEnabled() {
		return getWIFIManager().isWifiEnabled();
	}

	public void registerReceiver(View view) {
		view.getContext()
				.registerReceiver(new WifiReceiverSSIDs(), new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
	}

	public void refreshWiFi() {
		getWIFIManager().startScan();
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_wifisettings, container, false);
		OnClickFragments.registerTagFragment(root, this);
		return root;
	}

	@Override
	public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		try {
			if (Config.get().isUseWifiChecker()) {
				registerReceiver(view);
				refreshWiFi();
				fillSavedWIFIList();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addToListButton(View view) {
		String selectedSSID = getSelectedSSID();
		if (selectedSSID == null) {
			Drawer.appendToLog("Selected SSID is null", LogFragment.LogStatus.Error);
			return;
		}
		if (!Config.get().getSSIDList().contains(selectedSSID)) {
			addNewSSIDToSavedList(selectedSSID);
			fillSavedWIFIList();
		} else {
			Toast.makeText(Drawer.instance, "Already in list", Toast.LENGTH_SHORT).show();
		}
	}

	public void refreshWifiList(View view) {
		refreshWiFi();
	}

	public void removeSelectedFromSavedList(View view) {
		String selectedSavedSSID = getSelectedSavedSSID();

		if (selectedSavedSSID != null) {
			Config.get().getSSIDList().remove(selectedSavedSSID);
			fillSavedWIFIList();
		}
	}
}