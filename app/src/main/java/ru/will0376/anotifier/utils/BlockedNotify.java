package ru.will0376.anotifier.utils;

import android.text.TextUtils;
import com.google.gson.JsonObject;
import com.google.gson.annotations.Expose;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import ru.will0376.anotifier.Config;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
public class BlockedNotify {
	public static String[] checkContextIds = new String[]{"nt.summaryText", "nt.infoText", "nt.curTextLast", "nt.text"};
	@Expose
	int id;
	@Expose
	String packageName;
	@Expose
	String context;
	@Expose
	@Builder.Default
	List<Flags> flagsList = new ArrayList<>();

	/**
	 * Проверяет наличие id и packageName в конфиге с учетом флагов
	 *
	 * @return true - если условия не сходятся
	 */
	public static ReturnResult checkData(int id, String packageName) {
		for (BlockedNotify blockedNotify : Config.get().getBlockedNotifyList()) {

			boolean checkId = blockedNotify.getFlagsList().contains(Flags.AnyIDs);

			if (!checkId && id == blockedNotify.getId()) checkId = true;

			boolean checkPackage = blockedNotify.getFlagsList()
					.contains(Flags.PackageNameRegex) && packageName.matches(blockedNotify.getPackageName());

			if (!checkPackage && packageName.equals(blockedNotify.getPackageName())) checkPackage = true;

			if (checkId && checkPackage) {
				if (blockedNotify.getFlagsList().contains(Flags.ClearNotification))
					return ReturnResult.builder().foundNotify(blockedNotify).reason(RefusedReason.Clear).build();

				if (blockedNotify.getFlagsList().contains(Flags.UseContext) && blockedNotify.getFlagsList()
						.contains(Flags.UseContextRegex))
					return ReturnResult.builder().foundNotify(blockedNotify).reason(RefusedReason.CheckContext).build();

				return ReturnResult.builder().foundNotify(blockedNotify).reason(RefusedReason.CheckIdPackage).build();
			}
		}
		return ReturnResult.builder().reason(RefusedReason.None).build();
	}

	/**
	 * @return true - при совпадении контекста
	 */
	public static boolean checkContext(ReturnResult result) {
		JsonObject jo = result.getJo();
		if (result.getReason() != RefusedReason.CheckContext) {
			return false;
		}

		for (String s : checkContextIds) {
			if (jo.has(s)) {
				String asString = jo.get(s).getAsString();
				String context = result.getFoundNotify().context;
				if (result.getFoundNotify().getFlagsList().contains(Flags.UseContext)) {
					if (asString.contains(context)) return true;
				} else if (result.getFoundNotify().getFlagsList().contains(Flags.UseContextRegex)) {
					if (asString.matches(context)) return true;
				}
			}
		}
		//TODO: Понять, для чего нужен был этот todo
		//TODO
		return false;
	}

	public static void clearNotify(JsonObject jo) {
		for (String checkContextId : checkContextIds) {
			if (jo.has(checkContextId)) {
				jo.remove(checkContextId);
				jo.addProperty(checkContextId, "<Removed>");
			}
		}
	}

	public String getFormattedString() {
		return String.format("%s | %s | (%s)", id, packageName, TextUtils.join(",", flagsList));
	}

	@Getter
	public enum RefusedReason {
		CheckIdPackage,
		CheckContext,
		Clear,
		None
	}

	@Getter
	public enum Flags {
		PackageNameRegex("Use Regex in package name"),
		AnyIDs("Any ID's"),
		ClearNotification("Clear notification before sending"),
		UseContext("Use context"),
		UseContextRegex("Use regex for context");

		String normalName;

		Flags(String normalName) {
			this.normalName = normalName;
		}
	}

	@Builder
	@Getter
	@Setter
	public static class ReturnResult {
		RefusedReason reason;
		BlockedNotify foundNotify;
		JsonObject jo;
	}
}
