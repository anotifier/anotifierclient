
package ru.will0376.anotifier.tasks;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import io.netty.util.internal.StringUtil;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;

import java.util.TimerTask;

public class CheckConnectionTask extends TimerTask {
	public static boolean tryConnection = false;
	private static int errorCount = 1;
	public static int delay = 10000;

	public static void incrementErrorCount() {
		errorCount++;
	}

	public static void resetErrorCount() {
		errorCount = 1;
	}

	public static int getErrorCount() {
		return errorCount;
	}

	public static int getMaxErrorCount() {
		return Config.get().getMaxConnectionTry();
	}

	public static boolean canTryConnect() {
		return getMaxErrorCount() == -1 || !(errorCount > getMaxErrorCount());
	}

	public static String getCurrentSsid() {
		String ssid = null;
		ConnectivityManager cm = (ConnectivityManager) Drawer.instance.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = cm.getActiveNetworkInfo();
		if (networkInfo == null) {
			return null;
		}

		if (networkInfo.isConnected()) {
			final WifiManager wifiManager = (WifiManager) Drawer.instance.getSystemService(Context.WIFI_SERVICE);
			final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
			if (connectionInfo != null && !StringUtil.isNullOrEmpty(connectionInfo.getSSID())) {
				ssid = connectionInfo.getSSID();
			}
		}

		return ssid;
	}

	public static boolean checkWifi() {
		return Config.get().getSSIDList().stream().anyMatch(e -> e.split("\\|")[0].equals(getCurrentSsid()));
	}

	public static void reconnect() {
		boolean autoReconnect = Config.get().isAutoReConnect();
		boolean wifiChecker = Config.get().isUseWifiChecker();
		if (!ConnectionHelper.connected) {
			if (autoReconnect && !tryConnection) {
				String currentSsid = getCurrentSsid();
				if (currentSsid == null || currentSsid.isEmpty() || currentSsid.equals("<unknown>")) return;
				if (wifiChecker && !checkWifi()) return;
				tryConnection = true;
				Drawer.appendToLog(String.format("(%s/%s) Auto reconnect to server", getErrorCount(), getMaxErrorCount()), LogFragment.LogStatus.AutoReconnect);
				ConnectionHelper.connect();
			}
		}
	}

	@Override
	public void run() {
		if (!canTryConnect()) {
			if (ConnectionHelper.nettyClientThread != null) ConnectionHelper.disconnect(false);
			tryConnection = false;
			return;
		}
		reconnect();
	}
}
