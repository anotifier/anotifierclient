package ru.will0376.anotifier.utils.helpers;

import android.icu.text.SimpleDateFormat;
import android.icu.util.Calendar;
import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.ui.LogFragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class LogHelper {
	public static List<String> nullLog = new ArrayList<>();
	public static ScrollingMovementMethod movementMethod;
	public static final ArrayList<LogFragment.LogStatus> logDisabledStatues = new ArrayList<LogFragment.LogStatus>() {{
		add(LogFragment.LogStatus.CheckPermission);
	}};

	static {
		movementMethod = new ScrollingMovementMethod();
	}

	public static void lateAppendToLog(String text, LogFragment.LogStatus status) {
		if (!Drawer.debug) {
			if (logDisabledStatues.contains(status)) return;
		}
		TextView log = (LogFragment.instance == null ? null : LogFragment.instance.logger);
		String format = String.format("%s -> (%s) %s\n", new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(Calendar
				.getInstance()
				.getTime()), status, text);
		if (log == null) {
			nullLog.add(format);
		} else {
			if (log.getMovementMethod() != movementMethod) {
				log.setMovementMethod(movementMethod);
			}
			nullLog.forEach(log::append);
			log.append(format);
			nullLog.clear();
		}
	}
}
