package ru.will0376.anotifier.net;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.net.data.ResponseData;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.Base64Util;
import ru.will0376.anotifier.utils.helpers.EncryprionHelper;

public class ResponseHandler {
	public static JsonParser parser = new JsonParser();

	public static void handle(ResponseData requestData) {
		requestData.getAction().getHandler().setData(requestData).decodeData().process();
	}

	public abstract static class Handler {
		ResponseData requestDataIn;

		public abstract void process();

		public Handler setData(ResponseData data) {
			requestDataIn = data;
			return this;
		}

		public Handler decodeData() {
			try {
				String encData = requestDataIn.getEncData();
				String b64 = Base64Util.decode(encData);
				JsonObject parse = parser.parse(b64).getAsJsonObject();
				String s = EncryprionHelper.decryptText(EncryprionHelper.getServerEncKey(), Base64Util.decodeToByte(parse
						.get("encData")
						.getAsString()), Base64Util.decodeToByte(parse.get("iv").getAsString()));
				requestDataIn.setDecryptedData(s);
			} catch (Exception ex) {
				ex.printStackTrace();
			}
			return this;
		}
	}

	public static class HandlePong extends Handler {

		@Override
		public void process() {
			Drawer.appendToLog(requestDataIn.getDecryptedData(), LogFragment.LogStatus.Other);
		}
	}
}
