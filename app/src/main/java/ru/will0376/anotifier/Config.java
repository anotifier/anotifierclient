package ru.will0376.anotifier;

import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.stream.JsonReader;
import lombok.Getter;
import lombok.Setter;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.BlockedNotify;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
public class Config {
	public static boolean init = false;
	private static Config instance;
	private static File savedFile;
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	@Expose
	public String serverIp = "192.168.1.1:2015";
	@Expose
	public String serverSecretKey = "ServerKey";
	@Expose
	public String clientSecretKey = "ClientKey";
	@Expose
	public ArrayList<String> SSIDList = new ArrayList<>();
	@Expose
	public String uuid = UUID.randomUUID().toString();
	@Expose
	public boolean useWifiChecker = false;
	@Expose
	public boolean autoReConnect = false;
	@Expose
	public List<BlockedNotify> blockedNotifyList = new ArrayList<>();
	@Expose
	public int maxConnectionTry = 5;

	public static Config get() {
		return instance;
	}

	public static void init(File dir) {
		savedFile = new File(dir, "config.json");
		if (savedFile.exists()) {
			try {
				instance = load(savedFile);
			} catch (Exception e) {
				e.printStackTrace();
				Drawer.appendToLog(e.getLocalizedMessage(), LogFragment.LogStatus.Error);
				instance = new Config();
			}
		} else {
			instance = new Config();
			dir.mkdir();
		}
	}

	public static Config load(File file) throws Exception {
		return new GsonBuilder().setPrettyPrinting()
				.create()
				.fromJson(new JsonReader(new FileReader(file)), Config.class);
	}

	public String getCfgInString() {
		return new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create().toJson(this);
	}

	public void saveConfig() throws Exception {
		saveConfig(savedFile);
	}

	public void saveConfig(File file) throws Exception {
		if (!file.exists()) {
			file.createNewFile();
		}

		FileWriter f2 = new FileWriter(file, false);
		f2.write(getCfgInString());
		f2.close();
	}
}
