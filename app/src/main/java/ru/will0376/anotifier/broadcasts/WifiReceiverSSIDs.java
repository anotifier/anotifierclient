package ru.will0376.anotifier.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiManager;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.ui.WifiSettingsFragment;

public class WifiReceiverSSIDs extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		Drawer.appendToLog("Scan complete!", LogFragment.LogStatus.Wifi);
		boolean success = intent.getBooleanExtra(WifiManager.EXTRA_RESULTS_UPDATED, false);
		if (success) {
			WifiSettingsFragment.instance.fillSSIDList();
		} else {
			Drawer.appendToLog("WIFI scan error!", LogFragment.LogStatus.Wifi);
		}
	}
}
