package ru.will0376.anotifier.net.decoder;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import ru.will0376.anotifier.net.data.RequestData;

import java.nio.charset.StandardCharsets;

public class RequestDataEncoder extends MessageToByteEncoder<RequestData> {
	int i = 0;

	@Override
	protected void encode(ChannelHandlerContext ctx, RequestData msg, ByteBuf out) throws Exception {
		out.writeInt(i++);
		out.writeInt(msg.getDevice().ordinal());
		out.writeInt(msg.getAction().ordinal());
		writeString(msg.getEncData(), out);
		writeString(msg.getUuid(), out);
	}

	public void writeString(String wrote, ByteBuf byteBuf) {
		byteBuf.writeInt(wrote.length());
		byteBuf.writeCharSequence(wrote, StandardCharsets.UTF_8);
	}
}
