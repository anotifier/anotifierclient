package ru.will0376.anotifier.net.data;

import lombok.Data;
import lombok.Getter;
import ru.will0376.anotifier.net.ResponseHandler;

@Data
public class ResponseData {
	Action action;
	String encData;
	String decryptedData;

	@Getter
	public enum Action {
		Pong(new ResponseHandler.HandlePong());
		ResponseHandler.Handler handler;

		Action(ResponseHandler.Handler handler) {
			this.handler = handler;
		}
	}
}
