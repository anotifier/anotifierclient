package ru.will0376.anotifier.utils;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Switch;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import org.jetbrains.annotations.NotNull;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.R;
import ru.will0376.anotifier.ui.NotificationBlackListFragment;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class BlackListDialogFragment extends DialogFragment {
	@NonNull
	@NotNull
	@Override
	public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = requireActivity().getLayoutInflater();
		LinearLayout inflate = (LinearLayout) inflater.inflate(R.layout.dialog_notify_blacklist, null);
		Map<BlockedNotify.Flags, Switch> switches = new HashMap<>();

		for (BlockedNotify.Flags value : BlockedNotify.Flags.values()) {
			Switch aSwitch = new Switch(getContext());
			aSwitch.setText(value.getNormalName());
			int i = View.generateViewId();
			aSwitch.setId(i);
			inflate.addView(aSwitch);
			switches.put(value, aSwitch);
		}
		builder.setView(inflate).setPositiveButton(R.string.dialog_add, (dialog, id) -> {

			String blackListId = String.valueOf(((EditText) inflate.findViewById(R.id.blackListId)).getText());
			String packageName = String.valueOf(((EditText) inflate.findViewById(R.id.packageName)).getText());
			String context = String.valueOf(((EditText) inflate.findViewById(R.id.context)).getText());
			List<BlockedNotify.Flags> flagsList = switches.entrySet()
					.stream()
					.filter(e -> e.getValue().isChecked())
					.map(Map.Entry::getKey)
					.collect(Collectors.toList());

			BlockedNotify blockedNotify = BlockedNotify.builder()
					.id(Integer.parseInt(blackListId))
					.packageName(packageName)
					.context(context)
					.flagsList(flagsList)
					.build();
			Config.get().getBlockedNotifyList().add(blockedNotify);
			Drawer.fastSaveConfig();
			((NotificationBlackListFragment) getParentFragment()).fillList();
		}).setNegativeButton(R.string.dialog_cancel, (dialog, id) -> BlackListDialogFragment.this.getDialog().cancel());
		return builder.create();
	}
}
