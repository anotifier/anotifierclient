package ru.will0376.anotifier.utils;

import androidx.annotation.NonNull;
import org.jetbrains.annotations.NotNull;

public class ExceptionHandler implements Thread.UncaughtExceptionHandler {

	@Override
	public void uncaughtException(@NonNull @NotNull Thread t, @NonNull @NotNull Throwable e) {
		//TODO
		e.printStackTrace();
	}
}
