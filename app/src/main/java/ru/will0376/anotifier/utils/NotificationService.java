package ru.will0376.anotifier.utils;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.widget.RemoteViews;
import com.google.gson.JsonObject;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.net.data.RequestData;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;

import static ru.will0376.anotifier.net.data.RequestData.Action.NewNotify;

public class NotificationService extends NotificationListenerService {
	private final String TAG = this.getClass().getSimpleName();
	Context context;
	Helper helper;

	//adb shell cmd notification allow_listener com.mypackage.app/com.mypackage.app.mylistener
	public static Intent getIntentNotListSettings() {
		String tmp = "android.settings.ACTION_NOTIFICATION_LISTENER_SETTINGS";
		if (Build.VERSION.SDK_INT >= 22) {
			tmp = Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS;
		}
		return new Intent(tmp);
	}

	@Override
	public void onCreate() {
		super.onCreate();
		context = getApplicationContext();
		helper = new Helper();
		this.onListenerConnected();

	}

	@Override
	public void onNotificationPosted(StatusBarNotification sbn) {
		try {
			int id = sbn.getId();
			String packageName = sbn.getPackageName();
			BlockedNotify.ReturnResult returnResult = BlockedNotify.checkData(id, packageName);
			if (returnResult.getReason() != BlockedNotify.RefusedReason.None) {
				if (returnResult.getReason() == BlockedNotify.RefusedReason.CheckIdPackage) {
					if (Drawer.debug) Drawer.appendToLog("Skip " + packageName, LogFragment.LogStatus.CatchNotify);
					return;
				}
			}

			JsonObject jsonObject = helper.notificationToString(sbn);
			returnResult.setJo(jsonObject);
			if (returnResult.getReason() == BlockedNotify.RefusedReason.CheckContext && BlockedNotify.checkContext(returnResult)) {
				if (returnResult.getReason() == BlockedNotify.RefusedReason.Clear) {
					if (Drawer.debug) Drawer.appendToLog("Cleared " + packageName, LogFragment.LogStatus.CatchNotify);
					BlockedNotify.clearNotify(jsonObject);
				} else {
					if (Drawer.debug) Drawer.appendToLog("Skip " + packageName, LogFragment.LogStatus.CatchNotify);
					return;
				}
			}
			if (ConnectionHelper.connected) {
				ConnectionHelper.nettyClient.handler.sendMessage(RequestData.builder()
						.action(NewNotify)
						.encData(jsonObject.toString())
						.encryptClientData()
						.setUUID()
						.build());
			}
			if (jsonObject.has("hassmallIcon")) {
				jsonObject.remove("smallIconB64");
				jsonObject.addProperty("smallIconB64", "Removed");
			}
			if (jsonObject.has("haslargeIcon")) {
				jsonObject.remove("largeIconB64");
				jsonObject.addProperty("largeIconB64", "Removed");
			}
			String s = jsonObject.toString();
			Log.d(TAG, "notificationToString: " + s);
			Drawer.appendToLog(s, LogFragment.LogStatus.CatchNotify);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public class Helper {
		private PackageManager pm = null;

		public JsonObject notificationToString(StatusBarNotification statusBarNotification) {
			JsonObject jo = new JsonObject();
			Notification notification = statusBarNotification.getNotification();
			String curTitle = null;
			String curText = null;
			int flags = notification.flags;
			int progress;
			StringBuilder builder;
			String summaryText;
			byte hasPic;
			String curTextLast;
			String infoText;
			String actions = null;
			int extras;
			boolean hasImage = false;
			label124:
			{
				byte var4 = -1;
				if (notification.extras != null) {
					curTitle = this.getExtraString(notification.extras, "android.title.big", null);
					if (curTitle == null) {
						curTitle = this.getExtraString(notification.extras, "android.title", null);
					}

					curText = this.getExtraString(notification.extras, "android.text", null);
					curTextLast = this.getExtraString(notification.extras, "android.subText", null);
					if (curTextLast != null) {
						if (curText != null) {
							builder = new StringBuilder();
							builder.append(curText);
							builder.append("\n");
							builder.append(curTextLast);
							curTextLast = builder.toString();
						}

						curText = curTextLast;
					}

					summaryText = this.getExtraString(notification.extras, "android.summaryText", "");
					infoText = this.getExtraString(notification.extras, "android.infoText", "");
					CharSequence[] textLines = notification.extras.getCharSequenceArray("android.textLines");

					if (textLines == null) {
						curTextLast = "";
					} else {
						StringBuilder stringBuilder = new StringBuilder();

						for (int i = 0; i < textLines.length; ++i) {
							if (i != 0) {
								stringBuilder.append("\n");
							}

							stringBuilder.append(textLines[i]);
						}

						curTextLast = stringBuilder.toString();
					}

					if (notification.extras.getBoolean("android.progressIndeterminate", false)) {
						extras = 101;
					} else {
						progress = notification.extras.getInt("android.progress", 0);
						int progressMax = notification.extras.getInt("android.progressMax", 0);
						extras = var4;
						if (progress <= progressMax) {
							if (progressMax > 0) {
								extras = 100 / progressMax * progress;
							}
						}
					}

					int var22 = flags;
					if (!notification.extras.getBoolean("android.showWhen", true)) {
						var22 = flags | 256;
					}

					flags = var22;
					if (notification.extras.getBoolean("android.showChronometer", false)) {
						flags = var22 | 512;
					}

					if (notification.extras.containsKey("android.picture") && notification.extras.get("android.picture") != null) {
						actions = curTextLast;
						curTextLast = summaryText;
						hasPic = 1;
						summaryText = actions;
						break label124;
					}

					actions = curTextLast;
					curTextLast = summaryText;
					summaryText = actions;
				} else {
					curTextLast = "";
					infoText = curTextLast;
					summaryText = curTextLast;
					extras = var4;
				}

				hasPic = 0;
			}

			if (curTitle == null && curText == null) {
				JsonObject text = this.getText(notification);
				curTitle = text.get("title").getAsString();
				curText = text.get("text").getAsString();

				boolean error = text.get("error").getAsBoolean();
				if (error) jo.addProperty("error", error);
			}

			if (notification.actions != null) {
				StringBuilder stringBuilder = new StringBuilder();

				for (progress = 0; progress < notification.actions.length; ++progress) {
					if (progress > 0) {
						stringBuilder.append("|");
					}

					stringBuilder.append(notification.actions[progress].title);
				}

				actions = stringBuilder.toString();
			}

			if (notification.extras.containsKey("android.reduced.images")) {
				hasImage = notification.extras.getBoolean("android.reduced.images");

			}


			jo.addProperty("sbn.id", statusBarNotification.getId());
			jo.addProperty("sbn.tag", statusBarNotification.getTag());
			jo.addProperty("sbn.packageName", statusBarNotification.getPackageName());
			jo.addProperty("nt.time", notification.when);
			jo.addProperty("nt.flags", flags);
			jo.addProperty("nt.priority", notification.priority);
			jo.addProperty("nt.extras", extras);
			jo.addProperty("nt.appLabel", this.getAppLabel(statusBarNotification.getPackageName()));
			jo.addProperty("nt.tickerText", notification.tickerText == null ? null : notification.tickerText.toString());
			jo.addProperty("nt.title", curTitle);
			jo.addProperty("nt.text", curText);
			jo.addProperty("nt.actions", actions);
			jo.addProperty("nt.infoText", infoText);
			jo.addProperty("nt.curTextLast", curTextLast);
			jo.addProperty("nt.summaryText", summaryText);
			jo.addProperty("nt.hasPic", hasPic == 1);
			jo.addProperty("nt.hasImage", hasImage);
			//TODO: Maybe.
			//addIconToJson(jo, statusBarNotification, "largeIcon", notification.getLargeIcon());
			//addIconToJson(jo, statusBarNotification, "smallIcon", notification.getSmallIcon());
			return jo;
		}

		private void addIconToJson(JsonObject jo, StatusBarNotification statusBarNotification, String name, Icon iconIn) {
			boolean hasIcon = false;
			try {
				Resources resourcesForApplication = getBaseContext().getPackageManager()
						.getResourcesForApplication(statusBarNotification.getPackageName());
				Class<? extends Icon> aClass = iconIn.getClass();
				if (aClass != null) {
					Field mInt1 = aClass.getDeclaredField("mInt1");
					mInt1.setAccessible(true);
					int anInt = mInt1.getInt(iconIn);
					Drawable drawable = resourcesForApplication.getDrawable(anInt);
					if (drawable instanceof BitmapDrawable) {
						BitmapDrawable bitDw = ((BitmapDrawable) drawable);
						Bitmap bitmap = bitDw.getBitmap();
						ByteArrayOutputStream stream = new ByteArrayOutputStream();
						bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
						byte[] bitmapByte = stream.toByteArray();

						jo.addProperty(name + "B64", Base64Util.encode(bitmapByte));
						hasIcon = true;
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			jo.addProperty("has" + name, hasIcon);
		}

		private String getExtraString(Bundle bundle, String key, String elseString) {
			Object var4 = bundle.get(key);
			return var4 != null ? var4.toString() : elseString;
		}

		private JsonObject getText(Notification var1) {
			String curTitle = null;
			String curText = null;
			boolean error = false;
			RemoteViews contentView = var1.contentView;
			if (contentView != null) {
				label166:
				{
					Exception exception;
					label170:
					{
						Field[] fields;
						try {
							fields = contentView.getClass().getDeclaredFields();
						} catch (Exception ex) {
							exception = ex;
							break label170;
						}

						int var2 = 0;

						label161:
						while (true) {
							label171:
							{
								try {
									if (var2 >= fields.length) {
										break label166;
									}

									if (!fields[var2].getName().equals("mActions")) {
										break label171;
									}
								} catch (Exception var24) {
									exception = var24;
									error = true;
									break;
								}

								Iterator iterator;
								try {
									fields[var2].setAccessible(true);
									iterator = ((ArrayList) fields[var2].get(contentView)).iterator();
								} catch (Exception var16) {
									exception = var16;
									error = true;
									break;
								}

								label153:
								while (true) {
									Object var26;
									while (true) {
										while (true) {
											int var5 = -1;
											do {
												int var4;
												Object var10;
												Field[] var11;
												try {
													if (!iterator.hasNext()) {
														break label153;
													}

													var10 = iterator.next();
													var11 = var10.getClass().getDeclaredFields();
													var4 = var11.length;
												} catch (Exception var17) {
													exception = var17;
													error = true;
													break label161;
												}

												var5 = -1;
												var26 = var5;

												Object var6;
												for (int var3 = 0; var3 < var4; var26 = var6) {
													Field var12 = var11[var3];

													label175:
													{
														try {
															var12.setAccessible(true);
															if (var12.getName().equals("value")) {
																var6 = var12.get(var10);
																break label175;
															}
														} catch (Exception var19) {
															exception = var19;
															error = true;
															break label161;
														}

														var6 = var26;

														try {
															if (!var12.getName().equals("type")) {
																break label175;
															}

															var5 = var12.getInt(var10);
														} catch (Exception var18) {
															exception = var18;
															error = true;
															break label161;
														}

														var6 = var26;
													}

													++var3;
												}
											} while (var5 == -1);

											try {
												if (var5 == 9 || var5 == 10) {
													break;
												}
											} catch (Exception var20) {
												exception = var20;
												error = true;
												break label161;
											}
										}

										try {
											if (curTitle == null) {
												break;
											}
										} catch (Exception var21) {
											exception = var21;
											error = true;
											break label161;
										}

										if (var26 != null) {
											try {
												if (curText == null) {
													curText = var26.toString();
													continue;
												}
											} catch (Exception var23) {
												exception = var23;
												error = true;
												break label161;
											}

											String var29;
											StringBuilder var30;
											label140:
											{
												label139:
												{
													try {
														var30 = new StringBuilder();
														var30.append(curText);
														if (curText.length() > 0 && var26.toString().length() > 0) {
															break label139;
														}
													} catch (Exception var22) {
														exception = var22;
														error = true;
														break label161;
													}

													var29 = "";
													break label140;
												}

												var29 = "\r\n";
											}

											try {
												var30.append(var29);
												var30.append(var26);
												curText = var30.toString();
											} catch (Exception var13) {
												exception = var13;
												error = true;
												break label161;
											}
										}
									}

									if (var26 != null) {
										try {
											curTitle = var26.toString();
										} catch (Exception var15) {
											exception = var15;
											error = true;
											break label161;
										}
									} else {
										try {
											curTitle = "";
										} catch (Exception var14) {
											exception = var14;
											error = true;
											break label161;
										}
									}
								}
							}

							++var2;
						}
					}
					exception.printStackTrace();
				}
			}

			if (curTitle == null) {
				curTitle = "";
			}

			if (curText == null) {
				curText = "";
			}
			JsonObject jsonObject = new JsonObject();
			jsonObject.addProperty("title", curTitle);
			jsonObject.addProperty("text", curText);
			jsonObject.addProperty("error", error);
			return jsonObject;
		}

		private String getAppLabel(CharSequence var1) {
			if (this.pm == null) this.pm = NotificationService.this.getPackageManager();

			try {
				return this.pm.getApplicationLabel(this.pm.getApplicationInfo((String) var1, 0)).toString();
			} catch (Exception var2) {
				return "";
			}
		}
	}
}
