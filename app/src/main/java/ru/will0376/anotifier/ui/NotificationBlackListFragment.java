package ru.will0376.anotifier.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import org.jetbrains.annotations.NotNull;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.R;
import ru.will0376.anotifier.utils.BlackListDialogFragment;
import ru.will0376.anotifier.utils.BlockedNotify;
import ru.will0376.anotifier.utils.OnClickFragments;

import java.util.stream.Collectors;

public class NotificationBlackListFragment extends Fragment {
	Spinner spinner;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
	}

	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View root = inflater.inflate(R.layout.fragment_blacklist_notification, container, false);
		OnClickFragments.registerTagFragment(root, this);
		spinner = root.findViewById(R.id.notblacklist);
		if (spinner != null) {
			fillList();
		}

		return root;
	}

	@Override
	public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
	}

	public void fillList() {
		if (spinner != null) {
			ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, Config
					.get()
					.getBlockedNotifyList()
					.stream()
					.map(BlockedNotify::getFormattedString)
					.collect(Collectors.toList()));
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinner.setAdapter(adapter);
		}
	}

	public void addNew(View view) {
		BlackListDialogFragment blackListDialogFragment = new BlackListDialogFragment();
		blackListDialogFragment.show(getChildFragmentManager(), "addNew");
	}

	public void removeSelected(View view) {
		if (spinner != null) {
			String text = (String) spinner.getSelectedItem();
			if (text != null && !text.isEmpty() && text.contains("|")) {
				String[] split = text.split("|");
				int id = Integer.parseInt(split[0]);
				String packageName = split[1];
				Config.get()
						.getBlockedNotifyList()
						.removeIf(e -> e.getId() == id && e.getPackageName().equals(packageName));
				Drawer.fastSaveConfig();
			}
		}
	}
}
