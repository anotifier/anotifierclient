package ru.will0376.anotifier.utils.helpers;

import android.widget.Toast;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.net.NettyClient;
import ru.will0376.anotifier.net.data.RequestData;
import ru.will0376.anotifier.tasks.CheckConnectionTask;
import ru.will0376.anotifier.ui.LogFragment;

import java.util.Timer;

import static ru.will0376.anotifier.Drawer.appendToLog;

public class ConnectionHelper {
	public static NettyClient nettyClient;
	public static Thread nettyClientThread;
	public static boolean connected = false;
	public static Timer timer;
	public static CheckConnectionTask timerTask = new CheckConnectionTask();

	public static void connect() {
		Drawer.handler.post(() -> {
			try {
				String serverIp = Config.get().getServerIp();
				appendToLog("Trying connect to: " + serverIp, LogFragment.LogStatus.Connection);
				if (nettyClient == null) nettyClient = new NettyClient(Config.get().getServerIp());
				if (nettyClientThread == null) nettyClientThread = new Thread(nettyClient);
				if (nettyClientThread.isAlive()) {
					Toast.makeText(Drawer.instance, "Already connected!", Toast.LENGTH_SHORT).show();
					return;
				}
				nettyClientThread.start();
				appendToLog("Started thread!", LogFragment.LogStatus.Connection);
			} catch (Exception ex) {
				appendToLog(ex.getLocalizedMessage(), LogFragment.LogStatus.Error);
				CheckConnectionTask.incrementErrorCount();
				ex.printStackTrace();
				CheckConnectionTask.tryConnection = false;
			}
		});
	}

	public static void sendPing() {
		if (connected) nettyClient.handler.sendMessage(RequestData.builder()
				.action(RequestData.Action.Ping)
				.encData("Ping")
				.setUUID()
				.encryptClientData()
				.build());
		else appendToLog("Not connected!", LogFragment.LogStatus.ConnectionError);
	}

	public static void disconnect(boolean reconnect) {
		Drawer.handler.post(() -> {
			if (nettyClient != null) nettyClient.stopAll();
			appendToLog("Disconnected!", LogFragment.LogStatus.Connection);
			Toast.makeText(Drawer.instance, "Disconnected!", Toast.LENGTH_SHORT).show();
			connected = false;
			if (reconnect) connect();
		});
	}
}
