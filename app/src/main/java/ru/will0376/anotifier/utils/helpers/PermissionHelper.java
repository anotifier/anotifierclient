package ru.will0376.anotifier.utils.helpers;

import android.Manifest;
import android.content.Intent;
import android.provider.Settings;
import pub.devrel.easypermissions.EasyPermissions;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.NotificationService;

public class PermissionHelper {
	public static String[] permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE,
			Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.INTERNET,
			Manifest.permission.ACCESS_WIFI_STATE, Manifest.permission.CHANGE_WIFI_STATE,
			Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.CHANGE_WIFI_MULTICAST_STATE,
			Manifest.permission.CHANGE_NETWORK_STATE, Manifest.permission.READ_PHONE_STATE,
			Manifest.permission.ACCESS_LOCATION_EXTRA_COMMANDS, Manifest.permission.ACCESS_FINE_LOCATION,
			Manifest.permission.ACCESS_NETWORK_STATE};

	public static void requestAppPermissions() {
		checkPermissions();

		String notificationListenerString = Settings.Secure.getString(Drawer.instance.getContentResolver(), "enabled_notification_listeners");
		if (!Drawer.reGrandNotPerm() && (notificationListenerString == null || !notificationListenerString.contains(Drawer.instance
				.getPackageName()))) openIntent(NotificationService.getIntentNotListSettings());
	}

	public static void openIntent(Intent in) {
		in.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		Drawer.instance.startActivity(in);
	}

	private static void checkPermissions() {
		Drawer instance = Drawer.instance;
		for (String permission : permissions) {
			LogFragment.addToPreLog(String.format("%s -> %s", permission.replace("android.permission.", ""), EasyPermissions
					.hasPermissions(instance, permission)), LogFragment.LogStatus.CheckPermission);
		}

		boolean b = EasyPermissions.hasPermissions(instance, permissions);
		if (!b) EasyPermissions.requestPermissions(instance, "For work", 100, permissions);
	}
}
