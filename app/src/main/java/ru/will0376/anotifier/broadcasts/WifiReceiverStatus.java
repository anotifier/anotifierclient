package ru.will0376.anotifier.broadcasts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import ru.will0376.anotifier.Config;
import ru.will0376.anotifier.tasks.CheckConnectionTask;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;

public class WifiReceiverStatus extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {
		ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo wifi = connMgr.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

		boolean isConnected = wifi != null && wifi.isConnectedOrConnecting();
		if (isConnected && ConnectionHelper.nettyClientThread != null && Config.get().isUseWifiChecker()) {
			CheckConnectionTask.reconnect();
		}
	}
}
