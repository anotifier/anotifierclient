package ru.will0376.anotifier.net;

import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import ru.will0376.anotifier.Drawer;
import ru.will0376.anotifier.net.data.RequestData;
import ru.will0376.anotifier.ui.LogFragment;
import ru.will0376.anotifier.utils.helpers.ConnectionHelper;

public class ClientHandler extends SimpleChannelInboundHandler<RequestData> {
	ChannelHandlerContext ctx;

	public void sendMessage(RequestData msgToSend) {
		if (ctx != null) {
			if (!msgToSend.isEncrypted())
				Drawer.appendToLog(String.format("Message with action: %s, id: %s - not encrypted", msgToSend.getAction(), msgToSend
						.getId()), LogFragment.LogStatus.Other);
			ChannelFuture cf = ctx.writeAndFlush(msgToSend);
			ctx.flush();
			if (!cf.isSuccess()) {
				System.out.println("Send failed: " + cf.cause());
			}
		}
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		this.ctx = ctx;
		Drawer.appendToLog("Connected! send ping", LogFragment.LogStatus.Connection);
		sendMessage(RequestData.builder()
				.action(RequestData.Action.Ping)
				.encData("null")
				.setUUID()
				.encryptClientData()
				.build());
		ConnectionHelper.connected = true;
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
		cause.printStackTrace();
		ctx.close();
		Drawer.appendToLog("Exception coughed! message: " + cause.getMessage(), LogFragment.LogStatus.Error);
	}

	@Override
	protected void channelRead0(ChannelHandlerContext arg0, RequestData msg) throws Exception {

	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		Drawer.appendToLog("Channel closed!", LogFragment.LogStatus.Connection);
		ConnectionHelper.disconnect(false);
	}
}
